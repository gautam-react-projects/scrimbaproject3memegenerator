# Meme Generator

## Built on React

### Hosting link : https://meme-generator-gp.netlify.app/

### Description : 

The application fetches top 100 meme templates and gives option to user to append user generated  custom text on the meme.

MEME API : https://api.imgflip.com/get_memes