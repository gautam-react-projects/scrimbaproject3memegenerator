import React from "react";

import "./Main.css";

export default function Main() {
  const [meme, setmeme] = React.useState({
    topText: "",
    bottomText: "",
    image: "https://i.imgflip.com/24y43o.jpg",
  });

  const [allmemes, setallmemes] = React.useState([]);

  function getMemeImage() {
    const index = Math.floor(Math.random() * allmemes.length);

    const url = allmemes[index].url;

    setmeme((prevmeme) => ({
      ...prevmeme,
      image: url,
    }));
  }

  React.useEffect(() => {
    fetch("https://api.imgflip.com/get_memes")
      .then((data) => data.json())
      .then((data) => setallmemes(data.data.memes));
  }, []);

  function handlechange(event) {
    const { name, value } = event.target;
    setmeme((perveiousmeme) => ({
      ...perveiousmeme,
      [name]: value,
    }));
  }

  return (
    <div className="main">
      <div className="input-text">
        <input
          className="toptext"
          placeholder="Top text"
          name="topText"
          value={meme.topText}
          onChange={handlechange}
        />

        <input
          className="bottomtext"
          placeholder="Bottom text"
          name="bottomText"
          value={meme.bottomText}
          onChange={handlechange}
        />
      </div>

      <button className="generatebutton" onClick={getMemeImage}>
        <h2>Get a new meme image 🖼</h2>
      </button>

      <div className="IMG-container">
        <img className="memeimg" src={meme.image}></img>
        <h2 className="Top-text">{meme.topText}</h2>
        <h2 className="Bottom-text">{meme.bottomText}</h2>
      </div>
    </div>
  );
}
